#!/bin/sh

# Get upstream version from changelog in the same directory
dpkg-parsechangelog -l $(dirname $0)/changelog | sed -rne 's/^Version: ([0-9.]+)[-+].*$$/\1/p'
